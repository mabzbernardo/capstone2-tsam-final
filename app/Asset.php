<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    //

    public function type(){
    	return $this->belongsTo("\App\Type");
    }

    public function borrow(){
    	return $this->belongsToMany("\App\Borrow");
    }
}
