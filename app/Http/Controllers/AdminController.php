<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\User;

use \App\Asset;

use \App\Ticket;

use \App\Type;

use \App\Support;
use \App\Status;

use \App\TicketSupport;
use \App\Concern;

use Auth;

class AdminController extends Controller
{     
    //

    public function admindashboard(){

        $users = User::all();

        return view('admindashboard.admindashboard', compact('users'));
    }

    public function addassets(){

        $assets = Asset::all();

        $types = Type::all();
        
        return view('admindashboard.addassets', compact('assets','types'));
    }

    public function doneticket(){

        $tickets = Ticket::all();

        return view('admindashboard.doneticket', compact('tickets'));
    }

    public function assignsupport(){
        
        $ticketsupport = TicketSupport::all();

        $ticket = Ticket::all();

        $tickets = Ticket::all(); 

        $support = Support::all();

        $concerns = Concern::all();
        $user = User::all();
        $status = Status::all();



        return view('admindashboard/assignsupport', compact('ticketsupport','ticket','support','tickets','concerns','user','status','support'));
    }

    public function assignsupportticket(Request $req){
    	$user = Auth::user();
    	// dd($req);

    	$newSupport = new TicketSupport;
    	$newSupport->support_id = $req->support_id;
    	$newSupport->ticket_id = $req->ticket_id;
    	$newSupport->save();

    	return redirect()->back();

	}

    public function supportcancel($id){
		
		$supportcancel = TicketSupport::find($id);
	
		$supportcancel->delete();
		
		return redirect()->back();
		
    }
    
    public function ticketcancel($id){
		
		$ticketcancel = Ticket::find($id);
	
		$ticketcancel->delete();
		
		return redirect()->back();
		
	}


    public function template(){
        
        return view('partial/template');
    }

    public function usertemplate(){
        
        return view('partial/usertemplate');
    }


    public function userdashboard(){

        return view('userdashboard/userdashboard');
    }

    public function landingpage()
    {
        return view('landingpage');
    }
}
