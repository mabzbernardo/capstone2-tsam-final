<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Type;
use \App\Asset;
use Auth;

class AssetController extends Controller
{
    //

    public function userdashboard(){

        $assets = Asset::all();

        return view('userdashboard.userdashboard', compact('assets'));
    }

    public function saveassets(Request $req){
    	$user = Auth::user();
    	// dd($req);

    	$newAsset = new Asset;
    	$newAsset->name = $req->name;
    	$newAsset->productCode = $req->productCode;
    	$newAsset->type_id = $req->type_id;
    	$newAsset->availability = $req->availability;
    	$newAsset->save();

    	return redirect()->back();

	}
	
	public function deleteassets($id){
		
		$assetToDelete = Asset::find($id);
	
		$assetToDelete->delete();
		
		return redirect()->back();
		
	}
	
	public function editasset($id){
        $asset = Asset::find($id);
        $types = Type::all();

        return view ('admindashboard.editasset', compact('asset', 'types'));
	}
	
	public function update($id, Request $req){
        //validate
    
        // dd($req);

        $asset = Asset::find($id);
        $asset->name = $req->name;
        $asset->productCode = $req->productCode;
        $asset->type_id = $req->type_id;
        $asset->availability = $req->availability;



        $asset->save();

     

        return redirect('/addassets');

    }

    public function search(Request $req){
       $assets = Asset::where('name', 'LIKE', '%' . $req->search . '%')->orWhere('productCode','LIKE', '%' . $req->search .'%')->get();
       $types = Type::all();
       if(count($assets)==0){
           Session::flash('message', 'No items found');  
       }
       return view('admindashboard.addassets', compact('assets','types'));
   }

    //sorting
   public function sort($sort){
       $assets = Asset::orderBy('availability',$sort)->get();
       $types = Type::all();

       return view('admindashboard.addassets', compact('assets','types'));

   }

         //filtering
    public function filter($id){
       $assets = Asset::where('type_id', $id)->get();
       $types = Type::all();

       return view('admindashboard.addassets', compact('assets','types'));
   }

}
