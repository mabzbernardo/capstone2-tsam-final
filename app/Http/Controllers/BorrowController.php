<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Borrow;

use \App\Type;

use \App\Asset;

use\App\Status;

use\App\User;

use Auth;

class BorrowController extends Controller
{
    //x

    public function borrowdashboard($id){

        $borrow = Borrow::all();

        $types = Type::all();

        $status = Status::all();
        $asset = Asset::find($id);

        return view('borrow', compact('borrow','types','status','asset'));
    }

    public function saveborrow(Request $req){

        $AssetBorrow = new Borrow;
        $AssetBorrow->quantity = $req->quantity;
        $AssetBorrow->asset_id = $req->asset_id;
        $AssetBorrow->status_id = $req->status_id;
        $AssetBorrow->type_id = $req->type_id;
        $AssetBorrow->user_id = $req->user_id;
        $AssetBorrow->save();

        return redirect('/borrowtable');

    }


    public function borrowasset($id){
        $asset = Asset::find($id);
        $types = Type::all();
        $status = Status::all();
        $user = User::all();

        return view ('borrow', compact('asset', 'types','status','user'));
    }
    

    public function borrowtable(){

        
        if(Auth::user()->role_id == 1){
            $borrow = Borrow::all();   
        }else{
            $borrow = Borrow::where("user_id",Auth::user()->id)->get();
        }
        

        return view('userdashboard.borrowtable', compact('borrow'));
    }

    public function deleteborrowassets($id){
		$assetToDelete = Borrow::find($id);

		return $assetToDelete->delete();
		
    }
    
    public function editborrowtable($id){
        $asset = Borrow::find($id);
        $types = Type::all();
        $status = Status::all();

        return view ('userdashboard.editborrowtable', compact('asset', 'types','status'));
    }

    public function update($id, Request $req){
        //validate
    
        // dd($req);

        $borrow = Borrow::find($id);
        $borrow->quantity = $req->quantity;
      



        $borrow->save();

     

        return redirect('/borrowtable');

    }
    



}
