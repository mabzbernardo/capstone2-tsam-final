<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Concern;

use \App\Borrow;

use \App\Type;

use \App\Status;

use \App\Ticket;


use Auth;


class TicketController extends Controller
{
    //

    public function viewticket(){

        
        return view('userdashboard.ticket');
    }

    public function createReportTicket($id){
        $borrow = Borrow::find($id);
        $concerns = Concern::all();
        $types = Type::all();
        $status = Status::all();

        $ticket = Ticket::all();

        return view ('userdashboard.ticket', compact('borrow', 'types','status','ticket','concerns'));
    }
    
    public function addticket(Request $req){
    	$user = Auth::user();
    	// dd($req);

    	$newticket = new Ticket;
    	$newticket->comment = $req->comment;
    	$newticket->concern_id = $req->Concernid;
        $newticket->user_id = $req->userid;
        $newticket->borrow_id = $req->borrowid;
        $newticket->status_id = $req->statusid;
    	$newticket->save();

    	return redirect()->back();

	}
}
