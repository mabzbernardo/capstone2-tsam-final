<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    public function tickets(){
    	return $this->belongsToMany('\App\Ticket');
    }
    public function ticket(){
    	return $this->belongsTo('\App\Ticket');
    }

    public function supports(){
    	return $this->belongsToMany('\App\TicketSupport');
    }
 
}
