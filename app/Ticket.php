<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public function supports(){
    	return $this->belongsToMany('\App\Support');
    }

    public function concern(){
    	return $this->belongsTo('\App\Concern');
    }

    public function user(){
    	return $this->belongsTo('\App\User');
    }

    public function status(){
    	return $this->belongsTo('\App\Status');
    }
    public function support(){
        return $this->belongsTo('\App\Support');
    }


}
