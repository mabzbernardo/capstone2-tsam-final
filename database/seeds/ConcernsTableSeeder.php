<?php

use Illuminate\Database\Seeder;

class ConcernsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('concerns')->delete();

        \DB::table('concerns')->insert(array(
        	0 => 
        	array (
        		'id' => 1,
        		'name' => 'Connection Failed',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	1 => 
        	array (
        		'id' => 2,
        		'name' => 'No Power',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	2 => 
        	array (
        		'id' => 3,
        		'name' => 'Network Error',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	)
        ));
    }
}
