<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('statuses')->delete();

        \DB::table('statuses')->insert(array(
        	0 => 
        	array (
        		'id' => 1,
        		'name' => 'Active',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	1 => 
        	array (
        		'id' => 2,
        		'name' => 'Pending',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	2 => 
        	array (
        		'id' => 3,
        		'name' => 'Done',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	)
        ));
    }
}
