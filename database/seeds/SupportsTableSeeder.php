<?php

use Illuminate\Database\Seeder;

class SupportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('supports')->delete();

        \DB::table('supports')->insert(array(
        	0 => 
        	array (
        		'id' => 1,
        		'name' => 'Ferimar Paroligan',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	1 => 
        	array (
        		'id' => 2,
        		'name' => 'Efren Almozara',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	2 => 
        	array (
        		'id' => 3,
        		'name' => 'Clark Perdido',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	3 => 
        	array (
        		'id' => 4,
        		'name' => 'Kim Musnit',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	4 => 
        	array (
        		'id' => 5,
        		'name' => 'Alpacino Gacusan',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	5 => 
        	array (
        		'id' => 6,
        		'name' => 'Jerwin Marquez',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	6 => 
        	array (
        		'id' => 7,
        		'name' => 'Denz Edera',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        ));
    }
}
