<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('types')->delete();

        \DB::table('types')->insert(array(
        	0 => 
        	array (
        		'id' => 1,
        		'name' => 'Router',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	1 => 
        	array (
        		'id' => 2,
        		'name' => 'Switch',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	),
        	2 => 
        	array (
        		'id' => 3,
        		'name' => 'AP',
        		'created_at' => NULL,
        		'updated_at' => NULL
        	)
        ));
    }
}
