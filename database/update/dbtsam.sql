/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.4.11-MariaDB : Database - b55capstone2tsam
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`b55capstone2tsam` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `b55capstone2tsam`;

/*Table structure for table `admins` */

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `admins` */

/*Table structure for table `assets` */

DROP TABLE IF EXISTS `assets`;

CREATE TABLE `assets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productCode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` bigint(20) unsigned NOT NULL,
  `availability` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assets_type_id_foreign` (`type_id`),
  CONSTRAINT `assets_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `assets` */

insert  into `assets`(`id`,`name`,`productCode`,`type_id`,`availability`,`created_at`,`updated_at`) values (1,'wifi','test',1,5678,'2020-02-26 15:20:57','2020-03-01 05:57:15'),(22,'12','12',1,1,'2020-03-01 23:16:57','2020-03-01 23:16:57');

/*Table structure for table `borrows` */

DROP TABLE IF EXISTS `borrows`;

CREATE TABLE `borrows` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `asset_id` bigint(20) unsigned NOT NULL,
  `status_id` bigint(20) unsigned NOT NULL,
  `type_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `borrows_asset_id_foreign` (`asset_id`),
  KEY `borrows_status_id_foreign` (`status_id`),
  KEY `borrows_type_id_foreign` (`type_id`),
  KEY `borrows_user_id_foreign` (`user_id`),
  CONSTRAINT `borrows_asset_id_foreign` FOREIGN KEY (`asset_id`) REFERENCES `assets` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `borrows_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `borrows_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `borrows_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `borrows` */

insert  into `borrows`(`id`,`quantity`,`asset_id`,`status_id`,`type_id`,`user_id`,`created_at`,`updated_at`) values (32,1,1,1,1,1,'2020-03-01 03:56:52','2020-03-01 03:56:52'),(33,12,1,1,1,1,'2020-03-01 04:36:47','2020-03-01 04:36:47'),(34,3,1,1,1,1,'2020-03-01 05:58:48','2020-03-01 05:58:48'),(35,1,1,1,1,1,'2020-03-01 06:08:42','2020-03-01 06:08:42'),(36,1,1,1,1,1,'2020-03-01 07:26:14','2020-03-01 07:26:14'),(37,2312,1,1,1,1,'2020-03-01 07:31:33','2020-03-01 07:31:33'),(38,3,1,1,1,1,'2020-03-01 23:51:02','2020-03-01 23:51:02'),(39,2,1,1,1,1,'2020-03-01 23:55:37','2020-03-01 23:55:37'),(40,2,1,1,1,1,'2020-03-03 03:21:13','2020-03-03 03:21:13'),(41,3,1,1,1,1,'2020-03-03 03:40:40','2020-03-03 03:40:40'),(42,1,22,1,2,1,'2020-03-03 03:47:07','2020-03-03 03:47:07'),(43,34,1,1,1,1,'2020-03-03 05:46:22','2020-03-03 05:46:22');

/*Table structure for table `concerns` */

DROP TABLE IF EXISTS `concerns`;

CREATE TABLE `concerns` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `concerns` */

insert  into `concerns`(`id`,`name`,`created_at`,`updated_at`) values (1,'Connection Failed',NULL,NULL),(2,'No Power',NULL,NULL),(3,'Network Error',NULL,NULL);

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2020_02_27_003217_create_roles_table',1),(5,'2020_02_27_004023_create_types_table',1),(6,'2020_02_27_004025_create_assets_table',1),(7,'2020_02_27_004053_create_statuses_table',1),(8,'2020_02_27_004060_create_borrows_table',1),(9,'2020_02_27_004102_create_supports_table',1),(10,'2020_02_27_004123_create_concerns_table',1),(11,'2020_02_27_004200_create_tickets_table',1),(12,'2020_02_27_004609_create_ticket_support_table',1),(13,'2020_02_25_095235_create_admins_table',2);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

/*Table structure for table `statuses` */

DROP TABLE IF EXISTS `statuses`;

CREATE TABLE `statuses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `statuses` */

insert  into `statuses`(`id`,`name`,`created_at`,`updated_at`) values (1,'Active',NULL,NULL),(2,'Pending',NULL,NULL),(3,'Done',NULL,NULL);

/*Table structure for table `supports` */

DROP TABLE IF EXISTS `supports`;

CREATE TABLE `supports` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `supports` */

insert  into `supports`(`id`,`name`,`created_at`,`updated_at`) values (1,'sample support',NULL,NULL),(2,'sample 232',NULL,NULL);

/*Table structure for table `ticket_supports` */

DROP TABLE IF EXISTS `ticket_supports`;

CREATE TABLE `ticket_supports` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `ticket_id` bigint(20) unsigned NOT NULL,
  `support_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `support_id` (`support_id`),
  KEY `ticket_id` (`ticket_id`),
  CONSTRAINT `ticket_supports_ibfk_1` FOREIGN KEY (`support_id`) REFERENCES `supports` (`id`),
  CONSTRAINT `ticket_supports_ibfk_2` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `ticket_supports` */

insert  into `ticket_supports`(`id`,`date`,`ticket_id`,`support_id`,`created_at`,`updated_at`) values (6,NULL,2,1,'2020-03-01 23:28:51','2020-03-01 23:28:51'),(7,NULL,2,1,'2020-03-03 01:27:37','2020-03-03 01:27:37'),(8,NULL,8,1,'2020-03-03 05:48:48','2020-03-03 05:48:48'),(9,NULL,2,1,'2020-03-03 05:54:15','2020-03-03 05:54:15'),(10,NULL,2,1,'2020-03-03 05:58:34','2020-03-03 05:58:34'),(12,NULL,9,2,'2020-03-03 06:38:55','2020-03-03 06:38:55');

/*Table structure for table `tickets` */

DROP TABLE IF EXISTS `tickets`;

CREATE TABLE `tickets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date DEFAULT NULL,
  `concern_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `borrow_id` bigint(20) unsigned NOT NULL,
  `status_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tickets_concern_id_foreign` (`concern_id`),
  KEY `tickets_user_id_foreign` (`user_id`),
  KEY `tickets_borrow_id_foreign` (`borrow_id`),
  KEY `tickets_status_id_foreign` (`status_id`),
  CONSTRAINT `tickets_borrow_id_foreign` FOREIGN KEY (`borrow_id`) REFERENCES `borrows` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tickets_concern_id_foreign` FOREIGN KEY (`concern_id`) REFERENCES `concerns` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tickets_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tickets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `tickets` */

insert  into `tickets`(`id`,`comment`,`date`,`concern_id`,`user_id`,`borrow_id`,`status_id`,`created_at`,`updated_at`) values (2,'frwer','2020-03-01',1,1,37,1,NULL,NULL),(3,'23423','2020-03-11',1,1,32,1,NULL,NULL),(4,'weqtwe',NULL,1,1,37,1,NULL,NULL),(7,'1',NULL,1,1,37,1,'2020-03-03 04:39:06','2020-03-03 04:39:06'),(8,'wert',NULL,1,1,32,1,'2020-03-03 05:46:48','2020-03-03 05:46:48'),(9,'1',NULL,1,1,32,2,'2020-03-03 06:37:11','2020-03-03 06:37:11');

/*Table structure for table `types` */

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `types` */

insert  into `types`(`id`,`name`,`created_at`,`updated_at`) values (1,'Router',NULL,NULL),(2,'Switch',NULL,NULL),(3,'AP',NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`) values (1,'admin','admin@admin.com',NULL,'$2y$10$lUP3yS8AJ.fzCxGxxG0sAOx90vN7AkSvO5fD.2GtaRS/D6tgV4FR2',NULL,'2020-02-28 02:05:54','2020-02-28 02:05:54'),(2,'admin2','admin2@admin.com',NULL,'$2y$10$cogTS67kB0tV.An5FJ3Yi.9RYzZIcyhXkis1LGXT2AQFmeGGFANi6',NULL,'2020-02-28 04:01:53','2020-02-28 04:01:53'),(3,'sample','sample1@mail.com',NULL,'$2y$10$ykTdtLX6w.Sq3xj8Y7G/HOeTmOUmyl3q5bahTg3dmyOVn3YAduU7y',NULL,'2020-02-29 18:44:22','2020-02-29 18:44:22');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
