@extends('partial.template') 

@section ('title','Tsam- Add Asset')

@section ('navtitle','Tsam Add Assets')

@section('button')


<h3>Add Asset</h3>

<div class="col-lg-6 offset-lg-3">
	<form action="/addassets" method="POST">
		@csrf
		<div class="form-group">
			<label for="name">Asset Name:</label>
			<input type="text" name="name" class="form-control">
		</div>
		<div class="form-group">
			<label for="productCode">Product Code:</label>
			<input type="text" name="productCode" class="form-control">
		</div>
		<div class="form-group">
			<label for="type_id">Type:</label>
			<select name="type_id" class="form-control">
				@foreach($types as $type)
				<option value="{{$type->id}}">{{$type->name}}</option>

				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="availability">Availability:</label>
			<input type="number" name="availability" class="form-control">
		</div>
		<button class="btn btn-primary" type="submit">Add Asset</button>
	</form>
</div>

@endsection

@section('table')


<h3>Assets</h3></br>
{{-- Search Bar --}}
<div class="d-flex justify-content-end">
	<div class="col-lg-5">
		<form class="p-3" action="/search" method="POST">
			@csrf
			<div class="input-group">
				<input type="text" name="search" class="form-control" placeholder="Search items by name, description">
					<div class="input-group-append">
						<button class="btn btn-info" type="submit">Submit</button>
					</div>
			</div>
		</form>
	</div>
</div>

<div class="d-flex">
	
{{-- Filter --}}

<div class="col-lg-2 border shadow">
	<h4 class="bg-green-200">Filter by Type</h4>
		<ul class="list-group ml-6">
			@foreach($types as $type)
				<li class="list-group-item hover:bg-green-200">
				<a href="/addassets/{{$type->id}}">{{$type->name}}</a>
				</li>
			@endforeach
				<li class="list-group-item hover:bg-green-200">
					<a href="/addassets">All</a>
				</li>
		</ul>
	<h4 class="bg-green-200">Sort by Availability</h4>
		<ul class="list-group ml-6 ">
			<li class="list-group-item hover:bg-green-200">
			<a href="/addassets/sort/asc">Lowest to Highest</a>
			</li>

			<li class="list-group-item hover:bg-green-200">
			<a href="/addassets/sort/desc">Highest to Lowest</a>
			</li>
		</ul>
</div>

@if(Session::has("message"))
	<h4>{{Session::get('message')}}</h4>
@endif


<div class="col-lg-10 offset-lg-1">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Id:</th>
				<th>Name:</th>
                <th>Product Code:</th>
                <th>Type Id:</th>
                <th>Availability:</th>
				<th>Action:</th>
			</tr>
		</thead>
		<tbody>
			@foreach($assets as $assets)
				<tr>
					<th>{{$assets->id}}</th>
					<th>{{$assets->name}}</th>
                    <th>{{$assets->productCode}}</th>
                    <th>{{$assets->type->name}}</th>
                    <th>{{$assets->availability}}</th>
					<th>
					<form action="/deleteasset/{{$assets->id}}" method="POST">
							@csrf
							@method('DELETE')	
							<button class="btn btn-danger" type="submit">DELETE</button>
					</form>
					
						<a href="/editasset/{{$assets->id}}" class="btn btn-info">Edit</a>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>


</div>

@endsection