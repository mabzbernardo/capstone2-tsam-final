@extends('partial.template') 

@section ('title','Tsam-Assign Support')

@section ('navtitle','Tsam Assign Support')

@section('button')
<h3 style="color: blue;">Assign</h3></br>

<div class="col-lg-6 offset-lg-3">
	<form action="/assignsupport" method="POST">
	@csrf
	<div class="form-group">
			<label for="support_id">Support Id:</label>
			<select name="support_id" class="form-control">
				@foreach($support as $support)
				<option value="{{$support->id}}">{{$support->name}}</option>

				@endforeach
			</select>
		</div>

		<div class="form-group">
			<label for="ticket_id">Ticket Id:</label>
			<select name="ticket_id" class="form-control">
				@foreach($tickets as $tickets)
				<option value="{{$tickets->id}}">{{$tickets->id}}</option>

				@endforeach
			</select>
		</div>
	
		<button class="btn btn-primary" type="submit">Assign Support</button>
	</form>
</div>  


@endsection

@section('table')


<h3 style="color: blue;">Tickets</h3></br>

<div class="col-lg-10 offset-lg-1">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Id:</th>
				<th>Comment:</th>
                <th>Date:</th>
                <th>Concern:</th>
                <th>User Id:</th>
                <th>Borrow Id:</th>
                <th>Status:</th>
				<th>Action:</th>
			</tr>
		</thead>
		<tbody>
			@foreach($ticket as $ticket)
				<tr>
					<th>{{$ticket->id}}</th>
					<th>{{$ticket->comment}}</th>
                    <th>{{$ticket->date}}</th>
                    <th>{{$ticket->concern->name}}</th>
                    <th>{{$ticket->user->name}}</th>
                    <th>{{$ticket->borrow_id}}</th>
                    <th>{{$ticket->status->name}}</th>
					<th><form action="/ticketcancel/{{$ticket->id}}" method="POST">
							@csrf
							@method('DELETE')		
							<button class="btn btn-danger" type="submit">Cancel</button>
					</form>
					
						<a href="" class="btn btn-info">Edit</a></th>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection

@section('map')

<h3 style="color: blue;">Support</h3></br>  

    <div class="col-lg-10 offset-lg-1">
	<table class="table table-striped">
		<thead>
			<tr>
                <th>Id:</th>
                <th>Date:</th>
                <th>Ticket Id:</th>
                <th>Support Id:</th>
                <th>Action:</th>
			</tr>
		</thead>
		<tbody>
			@foreach($ticketsupport as $ticketsupport)
				<tr>
					<th>{{$ticketsupport->id}}</th>
					<th>{{$ticketsupport->created_at}}</th>
                    <th>{{$ticketsupport->ticket_id}}</th>
                    <th>{{$ticketsupport->support_id}}</th>
					<th><form action="/supportcancel/{{$ticketsupport->id}}" method="POST">
							@csrf
							@method('DELETE')	
							<button class="btn btn-danger" type="submit">Cancel</button>
					</form>
					
						<a href="" class="btn btn-info">Edit</a></th>
            
                  
				</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection





