@extends('partial.template') 

@section ('title','Tsam- Ticket Processing')

@section ('navtitle','Tsam Ticket Processing')

@section('table')


<h3 style="color: blue;">Tickets</h3></br>

<div class="col-lg-10 offset-lg-1">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Id:</th>
				<th>Comment:</th>
                <th>Date:</th>
                <th>Concern Id:</th>
                <th>User Id:</th>
                <th>Borrow Id:</th>
                <th>Status Id:</th>
				<th>Action:</th>
			</tr>
		</thead>
		<tbody>
			@foreach($tickets as $tickets)
				<tr>
					<th>{{$tickets->id}}</th>
					<th>{{$tickets->comment}}</th>
                    <th>{{$tickets->date}}</th>
                    <th>{{$tickets->concern_id}}</th>
                    <th>{{$tickets->user_id}}</th>
                    <th>{{$tickets->borrow_id}}</th>
                    <th>{{$tickets->status_id}}</th>
					<th>
						<button class="btn btn-primary" type="submit">Done</button>
					</th>
				</tr>
			@endforeach
		</tbody>

	</table>
</div>


@endsection