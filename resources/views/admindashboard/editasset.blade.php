@extends('partial.template') 

@section ('title','Tsam- Add Asset')

@section ('navtitle','Tsam Add Assets')

@section('button')

<div class="col-lg-6 offset-lg-3">
	<form action="/editasset/{{$asset->id}}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('PATCH')
		<div class="form-group">
			<label for="name">Asset Name:</label>
			<input type="text" name="name" class="form-control" value="{{$asset->name}}">
		</div>
		<div class="form-group">
			<label for="productCode">Product Code:</label>
			<input type="text" name="productCode" class="form-control" value="{{$asset->productCode}}">
		</div>
    
		<div class="form-group">
			<label for="type_id">Type:</label>
			<select name="type_id" class="form-control">
				@foreach($types as $type)
				<option value="{{$type->id}}" {{$type->id == $asset->type_id ? "selected" : ""}}>{{$type->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="availability">Availability:</label>
			<input type="number" name="availability" class="form-control">
		</div>
		<button class="btn btn-primary" type="submit">Edit Asset</button>
	</form>
</div>


@endsection

