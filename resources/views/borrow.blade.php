@extends('partial.usertemplate') 

@section ('title','Tsam- Borrow Asset')

@section ('navtitle','Tsam Borrow Assets')
 

@section('items')

<h3>Borrow Assets</h3>

<div class="col-lg-6 offset-lg-3">
	<form action="/userborrow" method="POST" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label for="asset_id">Asset Name:</label>
			<input type="text" class="form-control" value="{{$asset->name}}">
			<input type="hidden" name="asset_id" value="{{$asset->id}}">
		</div>
		<div class="form-group">
			<label for="user_id">Borrower Name:</label>
			<input type="text" class="form-control" value="{{ Auth::user()->name }}">
			<input type="hidden" name="user_id" class="form-control" value="{{ Auth::user()->id }}">
		</div>

		<div class="form-group">
			<label for="status_id">Status:</label>
			<select name="status_id" class="form-control">
				@foreach($status as $status)
				<option value="{{$status->id}}">{{$status->name}}</option>

				@endforeach
			</select>
		</div>

    
		<div class="form-group">
			<label for="type_id">Type:</label>
			<select name="type_id" class="form-control">
				@foreach($types as $type)
				<option value="{{$type->id}}" {{$type->id == $asset->type_id ? "selected" : ""}}>{{$type->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="quantity">Quantity:</label>
			<input type="number" name="quantity" class="form-control">
		</div>
		<button class="btn btn-primary" type="submit">Borrow Asset</button>
	</form>
</div>


<script type="text/javascript">

	const addToCart = (id, itemName) =>{
		console.log(itemName)
		let quantity = document.querySelector("#quantity_"+id).value;
		alert(quantity + " of item " + itemName + " has been added to cart");

		// let data = new FormData;

		// data.append("_token", "{{ csrf_token() }}");
		// data.append("quantity", quantity);

		fetch("/addtocart/"+id,{
			method: "POST",
			body: {
				"_token": {{csrf_token()}},
				"quantity": quantity
			}
		}).then(res=>res.text())
		.then(res=>console.log(res))
	}

</script>




@endsection