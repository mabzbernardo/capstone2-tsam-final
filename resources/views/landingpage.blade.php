<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tsam</title>
    <link href="{{ asset('css/ladingpage.css') }}" rel="stylesheet">
  <script src="{{ asset('js/app.js') }}" defer></script>

  <link rel="stylesheet" href="{{ URL::asset('css/modal.css') }}" />  
  <link rel="stylesheet" href="{{ URL::asset('css/styles.css') }}" />

</head>

<body>
    {{-- <div>
        <a class="btn btn-info" href="loginview"></a>
    </div> --}}
  
    <div id="promo">
        <div class="jumbotron">
            <h1>Technical 
			Support Asset Management</h1>
            @guest
            <a class="btn btn-info btn-lg" role="button" href="login">Get Started</a>
            @endguest
            @auth
            @if(Auth::user()->role_id == 1)
                <a class="btn btn-info btn-lg" role="button" href="/admindashboard">Get Started</a>
            @else
                <a class="btn btn-info btn-lg" role="button" href="/userdashboard">Get Started</a>
            @endif
            @endauth
        </div>
    </div>
    
            </div>
        </div>
    </footer>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>