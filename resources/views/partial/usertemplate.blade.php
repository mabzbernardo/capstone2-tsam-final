
<html lang="en"> 
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="style.css">
  <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
  
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <script src="{{ asset('js/app.js') }}" defer></script>

  
  <link rel="stylesheet" href="{{ URL::asset('css/styles.css') }}" />

	<script>
		$(document).ready(function(){
			$(".hamburger").click(function(){
			   $(".wrapper").toggleClass("collapse");
			});
		});
	</script>
</head>

<body>

<div class="wrapper">
  <div class="top_navbar">
    <div class="hamburger">
       <div class="one"></div>
       <div class="two"></div>
       <div class="three"></div>
    </div>
    <div class="top_menu">
      <div class="logo">@yield('navtitle')</div>

      <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
      
      {{-- <ul>
        <li>
          <a href="#">
          <i class="fas fa-user"></i>
          </a>
        </li>
      </ul> --}}

    </div>
  </div>   
  
<div class="sidebar">
  <ul>
    <li><a href="../userdashboard">
      <span class="icon"><i class="fas fa-home"></i></span>
      <span class="title">Home</span></a></li>

 



    <div class="dropdown">
    <li><a  href="../borrowtable" class="active">
    <span class="icon"><i class="fas fa-book"></i></span>
      <span class="title">Asset borrow</span>
      </a>

      
    </li>
  </div>

  </ul>
</div>
  
<div class="main_container">

  <div class="item">
  @yield('items')
	</div>

  
</div>


	
  
</div>
	
</body>
</html>