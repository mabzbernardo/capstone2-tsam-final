@extends('partial.usertemplate') 

@section ('title','Tsam- Borrow Asset')

@section ('navtitle','Tsam Borrow Assets')


@section('items')

<h3>Borrow Assets</h3></br>



<div class="col-lg-10 offset-lg-1">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Id:</th>
				<th>Asset:</th>
                <th>Borrower:</th>
                <th>Status:</th>
                <th>Type Id:</th>
                <th>Quantity:</th>
				<th>Action:</th>
				<th></th>
				<th></th>
				
			</tr>
		</thead>
		<tbody>
			@foreach($borrow as $borrow)
				<tr>
					<th>{{$borrow->id}}</th>
					<th>{{$borrow->asset->name}}</th>
					<th>{{$borrow->user->name}}</th>
                    <th>{{$borrow->status->name}}</th>
                    <th>{{$borrow->type->name}}</th>
                    <th>{{$borrow->quantity}}</th>
					<th>
					<form action="/borrowtable/{{$borrow->id}}" method="POST"> 
							@csrf
							@method('DELETE')	
							<button class="btn btn-danger" type="submit">Cancel</button>
					</form></th>
					{{-- for fetch 'Cancel' --}}
					{{-- <button class="btn btn-danger" type="button" onclick="borrowtable({{$borrow->id}})">Cancel</button> --}}

					<th><a href="/editborrowtable/{{$borrow->id}}" class="btn btn-info">Edit</a></br></th>

					<th>	<a href="/createReportTicket/{{$borrow->id}}" class="btn btn-danger">Report</a></th>

						
				</tr>
			@endforeach
		</tbody>
	</table>
</div>

					

{{-- <script type="text/javascript">

	const borrowtable = (id)=>{
		console.log(id)
		// let quantity = document.querySelector("#quantity_"+id).value;
		// alert(quantity + " of item " + itemName + " has been added to cart");

		let data = new FormData;

		data.append("_token", "{{ csrf_token() }}");
		data.append("_method", "Delete");
		// data.append("quantity", quantity);

		fetch("/borrowtable/"+id,{
			method: "post",
			body: data
		}).then(res=>res.text())
		.then(res=>console.log(res))
	}

</script> --}}



@endsection