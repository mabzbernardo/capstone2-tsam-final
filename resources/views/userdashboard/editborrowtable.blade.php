@extends('partial.usertemplate') 

@section ('title','Tsam- Borrow Asset')

@section ('navtitle','Tsam Borrow Assets')
 

@section('items')

<h3>Borrow Assets</h3>

<div class="col-lg-6 offset-lg-3">
	<form action="/editborrowtable/{{$asset->id}}" method="POST" enctype="multipart/form-data">
		@csrf

        @method('PATCH')
		<div class="form-group">
			<label for="asset_id">Asset Name:</label>
			<input type="hidden" name="asset_id" class="form-control" value="{{$asset->id}}">
			<input type="text" class="form-control" value="{{$asset->name}}">
		</div>
	
		<div class="form-group">
			<label for="status_id">Status:</label>
			<select name="status_id" class="form-control">
				@foreach($status as $status)
				<option value="{{$status->id}}">{{$status->name}}</option>

				@endforeach
			</select>
		</div>

    
		<div class="form-group">
			<label for="type_id">Type:</label>
			<select name="type_id" class="form-control">
				@foreach($types as $type)
				<option value="{{$type->id}}" {{$type->id == $asset->type_id ? "selected" : ""}}>{{$type->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="quantity">Quantity:</label>
			<input type="number" name="quantity" class="form-control">
		</div>
		<button class="btn btn-primary" type="submit">Borrow Asset</button>
		
	</form>
</div>




@endsection