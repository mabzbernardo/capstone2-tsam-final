@extends('partial.usertemplate') 

@section ('title','Tsam- Report Asset')

@section ('navtitle','Tsam Report Assets')


@section('items')

<h3>Report Asset</h3>

<div class="col-lg-6 offset-lg-3">
	<form action="/addticket" method="POST">
		@csrf
		<div class="form-group">
			<label for="comment">Comment:</label>
			<input type="text" name="comment" class="form-control">
		</div>
		<div class="form-group">
			<label for="Concernid">Concern ID:</label>
			{{-- <input type="text" name="Concernid" class="form-control"> --}}
			<select name="Concernid" class="form-control">
				@foreach($concerns as $concern)
				<option value="{{$concern->id}}">{{$concern->name}}</option>

				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="userid">Type :</label>
			<select name="userid" class="form-control">
				@foreach($types as $type)
				<option value="{{$type->id}}">{{$type->name}}</option>

				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="borrowid">Borrower ID:</label>
			<input type="text" name="borrowid" class="form-control" value="{{$borrow->id}}">
			{{-- <input type="text" class="form-control" value="{{$borrow->name}}"> --}}
		</div>

        <div class="form-group">
			<label for="statusid">Status:</label>
			{{-- <input type="number" name="statusid" class="form-control"> --}}
			<select name="statusid" class="form-control">
				@foreach($status as $status)
				<option value="{{$status->id}}">{{$status->name}}</option>

				@endforeach
			</select>
		</div>
		<button class="btn btn-primary" type="submit">Report Asset</button>
	</form>
</div>


@endsection