@extends('partial.usertemplate') 

@section ('title','Tsam- Borrow Asset')

@section ('navtitle','Tsam Borrow Assets')


@section('items')

<h1 class="text-center py-5" style="color :#4360b5;">ALL ASSET</h1>




<div class="container">
	<div class="row">
        @foreach($assets as $asset)
        <div class="col-lg-4 my-2">
				<div class="card">
					<div class="card-body">
                        <h4 class="card-title">Asset Name: {{$asset->name}}</h4>
                        <p class="card-text">Product Code: {{$asset->productCode}}</p>
                        <p class="card-text">Type id: {{$asset->type_id}}</p>
                        <p class="card-text">Availability: {{$asset->availability}}</p>
					</div>
					<a href="/userborrow/{{$asset->id}}" class="btn btn-info">Borrow Assets</a>
			
				
				</div>
			</div>

			
     
				
		@endforeach	
	</div>
</div>



@endsection