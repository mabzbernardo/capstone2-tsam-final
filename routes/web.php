<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

	//edit borrowtable testing link


Route::get('/userborrow', 'BorrowController@borrowdashboard');

	//user borrowtable
Route::get('/borrowtable', 'BorrowController@borrowtable');
Route::delete('/borrowtable/{id}', 'BorrowController@deleteborrowassets');
Route::get('/editborrowtable/{id}', 'BorrowController@editborrowtable');
Route::patch('/editborrowtable/{id}', 'BorrowController@update');

	//get specific - borrow item for viewing only
Route::get('/userborrow/{id}', 'BorrowController@borrowasset');
Route::post('/userborrow', 'BorrowController@saveborrow');

	//create report ticket
Route::get('createReportTicket', 'TicketController@viewticket');
Route::get('/createReportTicket/{id}', 'TicketController@createReportTicket');
Route::post('/addticket', 'TicketController@addticket');
Route::delete('/ticketcancel/{id}', 'AdminController@ticketcancel');

	//login view
Auth::routes();
Route::get('/', 'AdminController@landingpage');

	//testing routes for template
Route::get('/template', 'AdminController@template');
Route::get('/usertemplate', 'AdminController@usertemplate');

	//landing page
Route::get('/landingpage', 'HomeController@landingpage');

	// for Seaching
Route::post('/search', 'AssetController@search');

	//for middleware
	Route::middleware("admin")->group(function(){
		//admindashboard view
		Route::get('/admindashboard', 'AdminController@admindashboard');
		
		//add assets
		Route::get('addassets', 'AdminController@addassets');
		Route::delete('/deleteasset/{id}', 'AssetController@deleteassets');
		Route::get('/editasset/{id}', 'AssetController@editasset');
		Route::patch('/editasset/{id}', 'AssetController@update');
		//managae asset
		Route::get('/editasset/{id}', 'AssetController@editasset');
		Route::post('/addassets', 'AssetController@saveassets');
		//ticket admin view
		Route::get('doneticket', 'AdminController@doneticket');

		//assign support
		Route::get('assignsupport', 'AdminController@assignsupport');
		Route::post('/assignsupport', 'AdminController@assignsupportticket');
		Route::delete('/supportcancel/{id}', 'AdminController@supportcancel');
	});

Route::middleware("user")->group(function(){


});

	//login view  loginview
Route::get('loginview', 'HomeController@loginview');


// for sorting
Route::get('/addassets/sort/{sort}', 'AssetController@sort');
// for filtering
Route::get('/addassets/{id}', 'AssetController@filter');


//userdashboard view
		Route::get('/userdashboard', 'AssetController@userdashboard');